CMAKE_MINIMUM_REQUIRED(VERSION 2.8)


ADD_LIBRARY(xmlParser SHARED xmlParser.cpp )

# Make sure the linker can find the  library once it is built. 
link_directories (${ALEX_SOURCE_DIR}/xmlParser) 

add_executable (xmlTest xmlTest.cpp) 


# Link the executable to the  library. 
target_link_libraries (xmlTest xmlParser)
