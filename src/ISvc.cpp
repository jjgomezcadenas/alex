#include "ISvc.h"
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TRandom.h>
#include <iostream>
#include <string>
#include <sstream>
 #include <CLHEP/Units/SystemOfUnits.h>


using std::string;
using std::endl; 
using std::cout;
using std::cin;
using std::vector;
using std::pair; 


namespace alex {
//--------------------------------------------------------------------
  ISvc::ISvc(int step, vector<double> sigma, int rblob) : fStep(step),
        fSigma(sigma), fRblob(rblob)
//--------------------------------------------------------------------
  { 
  }
//--------------------------------------------------------------------
  void ISvc::LoadEvent(const irene::Event* ievt)
//--------------------------------------------------------------------
  {
    fIevt = ievt;
    SelectElectrons();
    fTrueHits.clear();
    fIevt->FillHitVector(fTrueHits, "ACTIVE");
    GetSparseHits();
    GetSmearedHits();
    GetBlobs(); 
  }
//--------------------------------------------------------------------
  void ISvc::SelectElectrons()
//--------------------------------------------------------------------
  { 
    // Selects the 2 electrons with max energy in the event
    fBeta.clear();
    auto ie1 = SelectBetaMax(IBIG); // find index electron with max energy
    auto e1 =fIevt->Tracks().at(ie1)->GetParticle(); // pointer to electron
    fBeta.push_back(e1);

    auto ie2 = SelectBetaMax(ie1); // find index next electron not e1
    if (ie2 != IBIG) // found a second electron
    {
      auto e2 =fIevt->Tracks().at(ie2)->GetParticle();
     fBeta.push_back(e2);
    }
  }

//--------------------------------------------------------------------
  size_t ISvc::SelectBetaMax(size_t notThis ) const
//--------------------------------------------------------------------
  {
    double emax =0;
    size_t idx = IBIG;
    for (size_t it = 0; it < fIevt->Tracks().size(); ++it)
    {  
      if (it == notThis) //this electrons already found
        continue;

      const irene::Particle* ip= fIevt->Tracks().at(it)->GetParticle();
    
      if (ip->GetPDGcode() != 11) // not interested if not an electron.
        continue;

      if (ip->Energy() > emax)
      {
        emax = ip->Energy();
        idx = it;
      }
    } 
    return idx;
  }
//--------------------------------------------------------------------
  bool ISvc::SelectBetaP(const irene::Particle* beta) const
//--------------------------------------------------------------------
  {
    if (abs(beta->Momentum() -PMAX) < EPS)
      return true;
    else
      return false;
  }
//--------------------------------------------------------------------
  IHits ISvc::SelectHits(string hitType) const
//--------------------------------------------------------------------
  {
    IHits ihits;
    if (hitType == "True") 
      ihits = fTrueHits;
    else if (hitType == "Sparse")
      ihits = fSparseHits;
    else if (hitType == "Smeared")
      ihits = fSmearedHits;
    else
      {
        std::cout << "ERROR: hit type unknown" << std::endl;
            exit (EXIT_FAILURE);
      }
    return ihits;
  }

//--------------------------------------------------------------------
  void ISvc::DrawHits(std::string hitType, vector<double> hl, vector<double> hu) const
//--------------------------------------------------------------------
  {
    auto ihits = SelectHits(hitType);

    TCanvas *c1 = new TCanvas( "c1", "Hits", 200, 10, 600, 800 );
    gStyle->SetOptStat(1000000001);

    auto nhit = ihits.size();
    
    auto xl = hl[0];
    auto xu = hu[0];
    auto yl = hl[1];
    auto yu = hu[1];
    auto zl = hl[2];
    auto zu = hu[2];

    auto hxz = new TH2F("hxz", "Hits: x vs z", nhit, zl, zu, nhit, xl, xu);
    auto hyz =  new TH2F("hyz", "Hits: y vs z", nhit, zl, zu, nhit, yl, yu);
    auto hxy =  new TH2F("hxy", "Hits: x vs y", nhit, xl, xu, nhit, yl, yu);

    for (auto it=ihits.begin(); it!=ihits.end(); ++it)
    {
      auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

       hxz->Fill(xyzt.Z(), xyzt.X(), energy);
       hyz->Fill(xyzt.Z(), xyzt.Y(), energy);
       hxy->Fill(xyzt.X(), xyzt.Y(), energy);
    }

    
    c1->Divide(1,3);
    
    c1->cd(1);
    hxz->Draw("colz");
    c1->cd(2);
    hyz->Draw("colz");
    c1->cd(3);
    hxy->Draw("colz");
    c1->Update();

    string input ;
    cout << "Return to continue:\n>";
    getline(cin, input);

    delete hxz;
    delete hyz;
    delete hxy;
    delete c1;
 

  }
//--------------------------------------------------------------------
  string ISvc::PrintHits(string hitType) const
//--------------------------------------------------------------------
  {
    auto ihits = SelectHits(hitType);

    std::ostringstream s;
    s<<"{";
    int i=0;
    for (auto it=ihits.begin(); it!=ihits.end(); ++it)
    {
      auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

      s << " hit number " << i << endl;
      s << " x (mm)= " << xyzt.X() << " y (mm)= " <<  xyzt.Y() 
        << " z (mm) = " << xyzt.Z() << " energy (MeV) = " << energy << endl;
    }
    s << "}" << std::ends;
    return s.str();
  }

  //--------------------------------------------------------------------
  string ISvc::PrintBlobs() const 
//--------------------------------------------------------------------
  {
    
    std::ostringstream s;
    s<<"{";
    int i=0;
    for (auto it=fBlobs.begin(); it!=fBlobs.end(); ++it)
    {
      auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

      s << " blob number " << i << endl;
      s << " x (mm)= " << xyzt.X() << " y (mm)= " <<  xyzt.Y() 
        << " z (mm) = " << xyzt.Z() << " energy (keV) = " << energy/CLHEP::keV << endl;
    }
    s << "}" << std::ends;
    return s.str();
  }

//--------------------------------------------------------------------
  string ISvc::PrintElectrons() const 
//--------------------------------------------------------------------
  {
      
    std::ostringstream s;
    s<<"{";

    auto tkin =0.;
    for (size_t it = 0; it < fBeta.size(); ++it)
    {
      auto beta = fBeta[it];
      s << "electron: "<< it <<":\n";
      auto pi = beta->GetInitialMomentum();
      auto vi = beta->GetInitialVertex();
      auto vf = beta->GetDecayVertex();
      tkin += beta->Energy()-beta->GetMass();

      s << " pi (px,py,pz) MeV: =  " << pi[0] << "," << pi[1]<< "," << pi[2] << endl;
      s << " vi (x,y,z)  mm:    =  " << vi[0] << "," << vi[1]<< "," << vi[2] << endl;
      s << " vf (x,y,z)  mm:    =  " << vf[0] << "," << vf[1]<< "," << vf[2] << endl;  
      s << " P (MeV)=  " << beta->Momentum()
        << " E (MeV)  = "<< beta->Energy() 
        << " Kinetic (MeV)= " << beta->Energy()-beta->GetMass()<< endl;
    }
    s<< "2e kinetic energy = " << tkin <<endl;
    s << "}" << std::ends;
    return s.str();
  }

//--------------------------------------------------------------------
  string ISvc::PrintElectron(const irene::Particle* beta) const 
//--------------------------------------------------------------------
  {
      
    std::ostringstream s;
    s<<"{";
    auto pi = beta->GetInitialMomentum();
    auto vi = beta->GetInitialVertex();
    auto vf = beta->GetDecayVertex();

    s << " pi (px,py,pz) MeV: =  " << pi[0] << "," << pi[1]<< "," << pi[2] << endl;
    s << " vi (x,y,z)  mm:    =  " << vi[0] << "," << vi[1]<< "," << vi[2] << endl;
    s << " vf (x,y,z)  mm:    =  " << vf[0] << "," << vf[1]<< "," << vf[2] << endl;  
    s << " P (MeV)=  " << beta->Momentum()
      << " E (MeV)  = "<< beta->Energy() 
      << " Kinetic (MeV)= " << beta->Energy()-beta->GetMass()<< endl;
    
    s << "}" << std::ends;
    return s.str();
  }

//--------------------------------------------------------------------
  void ISvc::GetSparseHits()
//--------------------------------------------------------------------
  {
    int i=0;
    double edep =0;
    fSparseHits.clear();
    for (auto it=fTrueHits.begin(); it!=fTrueHits.end(); ++it)
    {
      edep += it->second;
      if (i==0 or i == fStep)
      {
        auto hit = *it;
        pair<TLorentzVector,double> nhit;
        nhit.first = it->first;
        nhit.second = edep;
        fSparseHits.push_back(nhit);
        
        i=0;
        edep=0;
      }
      i++;
    }
  }
//--------------------------------------------------------------------
  void ISvc::GetSmearedHits()
//--------------------------------------------------------------------
  
  {
    fSmearedHits.clear();
    for (auto it=fSparseHits.begin(); it!=fSparseHits.end(); ++it)
    {
      auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

      TLorentzVector sxyz;
      for (int i=0; i<3; ++i)
      {
        auto dx = gRandom->Gaus(0,1)*fSigma[i];
        sxyz[i] = xyzt[i]+dx;
      }
      sxyz[3]=xyzt[3];

      pair<TLorentzVector,double> nhit;
      nhit.first = sxyz;
      nhit.second = energy;
      fSmearedHits.push_back(nhit);
    }
  }
//--------------------------------------------------------------------
  void ISvc::GetBlobs()
//--------------------------------------------------------------------
  {
    fBlobs.clear(); 
    double edep =0.;
    for (size_t it=0; it< fRblob; ++it)
    {
      auto hit = fTrueHits[it];
      edep += hit.second;
    }
    {
      auto hit = fTrueHits[0];
      pair<TLorentzVector,double> nhit;
      nhit.first = hit.first;
      nhit.second = edep;
      fBlobs.push_back(nhit);
    }
    edep =0;
    for (size_t it=fTrueHits.size()-1; it> fTrueHits.size()-fRblob; --it)
    {
      auto hit = fTrueHits[it];
      edep += hit.second;
    }
    {
      auto hit = fTrueHits[fTrueHits.size()-1];
      pair<TLorentzVector,double> nhit;
      nhit.first = hit.first;
      nhit.second = edep;
      fBlobs.push_back(nhit);
    }
  }
//--------------------------------------------------------------------
 void ISvc::Info(ostream& s) const
//--------------------------------------------------------------------
  {
    s << std::endl;
    s<<"{ISvc: ";
    s << "step for sparsing=" << fStep << " smearing: sigma_x (mm) =" 
      << fSigma[0]  << " sigma_y (mm) =" << fSigma[1]
      << " sigma_y (mm) =" << fSigma[2] << " Blob takes " <<fRblob << "hits" 
      << std::endl;
    s << "}";
  }

  ostream& operator << (ostream& s, const ISvc& p) 
  {
    p.Info(s);
    return s; 
  }
}
    