#ifndef GENUTIL_
#define GENUTIL_
/*
 GENUTIL: Generic Utilities 
 JJGC, July, 2014.
*/

#include <string>
#include <vector>

namespace alex {
	std::string AddStrings(std::vector<std::string> sv);
	std::string FilePath(std::string dataPath,std::string fileName);

}
#endif