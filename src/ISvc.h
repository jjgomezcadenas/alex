#ifndef ISvc_
#define ISvc_
/*
 ISvc: Irene Svc
 JJGC, July, 2014.
*/

#include <irene/Event.h>
#include <irene/Track.h>
#include <irene/Particle.h>
#include <TLorentzVector.h>
#include <vector>
#include <utility>
#include <sstream>
#define PMAX 2.9135 //in MeV
#define EPS  1e-3
#define IBIG 999999

namespace alex {
	typedef std::pair<TLorentzVector,double> IHit;
	typedef std::vector<std::pair<TLorentzVector,double> > IHits;

	class ISvc {
	
  	public:

			ISvc(int step, std::vector<double> sigma, int rblob);
			void LoadEvent(const irene::Event* ievt);
			std::vector<const irene::Particle*> Electrons() const {return fBeta;}
			IHits SelectHits(std::string hitType) const; //true, sparse, smeared
			IHits Blobs() const {return fBlobs;}
			void DrawHits(std::string hitType, std::vector<double> hl, std::vector<double> hu) const;
			std::string PrintHits(std::string hitType) const; //true, sparse, smeared
			std::string PrintBlobs() const ;
			std::string PrintElectrons() const ; //"e1", "e2"
			std::string PrintElectron(const irene::Particle* beta) const ;
			std::vector<double> MeasurementErrors () const {return fSigma;}
			bool SelectBetaP(const irene::Particle* beta) const;
			void Info(std::ostream& s) const;
		private:
			void GetBlobs();
			void GetSparseHits();
			void GetSmearedHits();
			void SelectElectrons();
			size_t SelectBetaMax(size_t notThis ) const;

			const irene::Event* fIevt;
			std::vector<const irene::Particle*> fBeta;
			int fStep;
			std::vector<double> fSigma;
			int fRblob;
			IHits fTrueHits;
			IHits fSparseHits;
			IHits fSmearedHits;
			IHits fBlobs;

	};
	std::ostream& operator << (std::ostream& s, const ISvc& p);
}
#endif