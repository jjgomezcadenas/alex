
// ----------------------------------------------------------------------------
//  $Id: AConfig.cc 
//
//  Author:  <gomez@mail.cern.ch>
//  Created: July 2014
//  
//  Copyright (c) 2014 NEXT Collaboration
// ---------------------------------------------------------------------------- 

#include "AConfig.h"
#include <alex/xmlParser.h>
#include <cstdlib>


namespace alex {

  AConfig::AConfig(std::string xmlFile)
  {

    // this open and parse the XML file:
    XMLNode xMainNode=XMLNode::openFileHelper(xmlFile.c_str(),"PMML");

    // this gets the number of "DataField" tags:

    auto xDataDict=xMainNode.getChildNode("DataDictionary");
    int ndf=xDataDict.nChildNode("DataField");

    //printf ("number of data fields = %d\n",ndf );

    int i,myIterator=0;

    for (i=0; i<ndf; i++)
    {
        auto xDataField = xDataDict.getChildNode("DataField",&myIterator);
        auto name = xDataField.getAttribute("name");
        std::string dataType =  xDataField.getAttribute("dataType");
        auto xDataValue=xDataField.getChildNode("Value");
        auto value =  xDataValue.getAttribute("value");
        
        
        // std::cout << "name of data field =" << name 
        //           << " value = " << value 
        //           <<" dataType = " << dataType << std::endl;

        if (dataType=="double")
        {
          double dval = std::stod (value);
          //printf ("found a double: value = %7.1f\n", dval);
          fDoubleData[name]=dval;
        }
        else if (dataType=="integer")
        {
          int ival = std::stoi (value);
          //printf ("found an integer: value = %d\n", ival);
          fIntData[name]=ival;
        }

        else if (dataType=="string")
        {
          //std::cout << "found a string: value = "<< value << std::endl;
          fStringData[name]=value;
        }

        else
        {
          std::cout << "ERROR: data type unknown" << std::endl;
          exit (EXIT_FAILURE);
        }
    }

    
  }

  void AConfig::Info(ostream& s) const
  {
    s << std::endl;    
    s << "Int data:----" << std::endl;

    for (auto it=fIntData.begin(); it!=fIntData.end(); ++it)
    {
      s << "name = " << it->first << " value = " << it->second << std::endl;
    }

    s << "Double data:----" << std::endl;

    for (auto it=fDoubleData.begin(); it!=fDoubleData.end(); ++it)
    {
      s << "name = " << it->first << " value = " << it->second << std::endl;
    }

    s << "String data:----" << std::endl;

    for (auto it=fStringData.begin(); it!=fStringData.end(); ++it)
    {
      s << "name = " << it->first << " value = " << it->second << std::endl;
    }

  }
    
  ostream& operator << (ostream& s, const AConfig& p) 
  {
    p.Info(s);
    return s; 
  }
}

