#ifndef ACONFIG_
#define ACONFIG_
/*
 ACONFIG: Alex Configuration
 

 JJGC, July, 2014.
*/

#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <iostream>
#include <map>
#include <sstream>


using std::string; 
using std::cout; 
using std::endl; 
using std::ostream;
namespace alex {

class AConfig {
	public:
		AConfig(std::string xmlFile);
		virtual ~AConfig(){};

		void AddInteger(std::string key, int value){fIntData[key]=value;}
		void AddDouble(std::string key, double value){fDoubleData[key]=value;}
		void AddString(std::string key, string value){fStringData[key]=value;}

		double GetDouble(std::string key){return fDoubleData[key];}
		int GetInteger(std::string key){return fIntData[key];}
		std::string GetString(std::string key){return fStringData[key];}

		void Info(ostream& s) const;

	private:
		std::map<string,string> fStringData;
		std::map<string,int> fIntData;
		std::map<string,double> fDoubleData;
			
			
	};

	ostream& operator << (ostream& s, const AConfig& p);
}
#endif