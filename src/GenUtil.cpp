//general utilties (GUtil)

#include "GenUtil.h"
#include <sstream>


using std::string; 
using std::stringstream; 
using std::vector;

namespace alex {
  string AddStrings(vector<string> sv)
  {
    stringstream s;
    for (auto it=sv.begin(); it!=sv.end(); ++it)
    {
      s<< *it;
    }
    return s.str();
  }

  string FilePath(string dataPath,string fileName)
  {
    vector<string> sv;
    sv.push_back(dataPath);
    sv.push_back("/");
    sv.push_back(fileName);
    string fp = AddStrings(sv);
    return fp;
  }
}
