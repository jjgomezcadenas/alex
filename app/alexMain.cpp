// Simple driver for Alex
// Adapted for NEXT by J.J. Gomez-Cadenas, 2014

//MagBox_Xe_10atm_00tesla.Xe136_bb0nu.0.next
//MagBox_Xe_10atm_00tesla.e2447.next

#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>
#include <TApplication.h>
#include <TCanvas.h>

#include <stdio.h>
#include <string>     // std::string, std::stod
#include <vector>   
#include <algorithm>
#include <iostream>

#include <alex/xmlParser.h>
#include <alex/AConfig.h>
#include <alex/LogUtil.h>
#include <alex/GenUtil.h>
#include <alex/ISvc.h>
#include <alex/KFSetup.h>
#include <alex/KFSvc.h>
//#include <alex/RecPackUtil.h>

using std::string; 
using std::cout;
using std::cin; 
using std::endl; 
using std::vector;
using std::pair; 

using namespace alex;


int main(int argc, char **argv)
{
  // to display histos online
  TApplication* theApp = new TApplication("App", &argc, argv);


  //logger
  InitLogger();
  log4cpp::Category& klog = log4cpp::Category::getRoot();

  //configuration
  auto acf = AConfig("alexConfig.xml");

  cout << "printing configuration data" << endl;
  cout << acf << endl;

  // read data from xml file----------

  //data file path
  string dataPath = acf.GetString("dataPath");
  string fileName = acf.GetString("fileName");

  //debugging flags
  string debugLevel = acf.GetString("debugLevel");
  string drawLevel = acf.GetString("drawLevel");

  int nEvents = acf.GetInteger("nEvents");

  //smearing parameters
  int step = acf.GetInteger("step");
  double sigma = acf.GetDouble("sigma");

  //KF parameters
  string gas = acf.GetString("gas"); // gas: Xe, SeF6...
  string model = acf.GetString("model"); //trajectory model: helix, sline...
  double Pr = acf.GetDouble("Pr");  // Pressure in bar
  double B = acf.GetDouble("B");  // magnetic field in Tesla

  double maxChi2 = acf.GetDouble("maxChi2");  // max chi2 to break segment
  double maxOutliers = acf.GetDouble("maxOutliers");  
  double maxExtrapFailures = acf.GetDouble("maxExtrapFailures");  
  double minDistanceNodeOrdering = acf.GetDouble("minDistanceNodeOrdering");  
  double minGoodNodeOrdering = acf.GetDouble("minGoodNodeOrdering");  

  string fitterVerbosity = acf.GetString("fitterVerbosity");
  string navigationVerbosity = acf.GetString("navigationVerbosity");
  string modelVerbosity = acf.GetString("modelVerbosity");
  string matchingVerbosity = acf.GetString("matchingVerbosity");

  auto xl = acf.GetDouble("xl");
  auto xu = acf.GetDouble("xu");
  auto yl = acf.GetDouble("yl");
  auto yu = acf.GetDouble("yu");
  auto zl = acf.GetDouble("zl");
  auto zu = acf.GetDouble("zu");

  // done reading data from xml file ---------

  // general debug level
  SetDebugLevel(debugLevel);

  //read data file
  string fp = FilePath(dataPath,fileName);

  irene::Event* ievt = new irene::Event();
  TFile* ifile = new TFile(fp.c_str(), "READ");
  TTree* fEvtTree = dynamic_cast<TTree*>(ifile->Get("EVENT"));
  fEvtTree->SetBranchAddress("EventBranch", &ievt);

  auto nRun = std::min(nEvents, (int) fEvtTree->GetEntries());

  cout << "number of entries in Irene Tree = " << fEvtTree->GetEntries() << endl;
  cout << "number of events required = " << nEvents << endl;
  cout << "number of events to run  = " << nRun << endl;
      
  //Kalman Filter Setup
  auto kfs = KFSetup(gas, model, Pr, B);
  kfs.SetVerbosity(fitterVerbosity,navigationVerbosity,modelVerbosity,
                    matchingVerbosity);
  kfs.SetFitParameters (maxChi2,maxOutliers, maxExtrapFailures,
                        minDistanceNodeOrdering, minGoodNodeOrdering);


  klog << log4cpp::Priority::INFO << "KFSetup " << kfs;

  vector<double> vsigma;
  vsigma.push_back(sigma); // sigma_x
  vsigma.push_back(sigma); // sigma_y
  vsigma.push_back(0.01); // sigma_z ~0

  vector<double> hl; 
  vector<double> hu;

  hl.push_back(xl);
  hl.push_back(yl);
  hl.push_back(zl);

  hu.push_back(xu);
  hu.push_back(yu);
  hu.push_back(zu);

  int rblob = 10;

  auto isvc = ISvc(step,vsigma,rblob);

  cout << "ISVC--->" << isvc << endl;
  
  string input ;
  cout << "Return to continue:\n>";
  getline(cin, input);

  //init ReckPack

  int nb;

  auto kfsvc = new KFSvc(kfs);

  cout << "Return to continue:\n>";
  getline(cin, input);

  //-----------Event loop --------
  for (int ivt = 0; ivt < nRun; ivt++)
  {
    nb = fEvtTree->GetEntry(ivt);

    isvc.LoadEvent(ievt);

    klog << log4cpp::Priority::INFO 
        << "reading event  " << ivt;
    klog << log4cpp::Priority::INFO 
        << "reading " << nb  << "bytes from tree"; 

    klog << log4cpp::Priority::INFO 
        << " Electrons in event  " << isvc.PrintElectrons();

    cout << "Return to continue:\n>";
    getline(cin, input);  

    isvc.DrawHits("True", hl, hu);
    isvc.DrawHits("Sparse", hl, hu);
    isvc.DrawHits("Smeared", hl, hu);

    klog << log4cpp::Priority::INFO 
        << "Blobs--->" 
        << isvc.PrintBlobs();

    cout << "Return to continue:\n>";
    getline(cin, input);

    // fit only 
    bool test = kfsvc->Fit(isvc);

    klog << log4cpp::Priority::INFO 
        << " fit result  " << test << "\n"; 
    cout << "Return to continue:\n>";
    getline(cin, input);

  }
 
 
  //------------
    theApp->Run();
    return 0;
  }
  
  
