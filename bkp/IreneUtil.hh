#ifndef IUTIL_
#define IUTIL_
/*
 IreneUtil: Irene Utilities 
 JJGC, July, 2014.
*/

#include <irene/Event.h>
#include <irene/Track.h>
#include <irene/Particle.h>
#include <irene/Units.h>
#include <TLorentzVector.h>
 

namespace alex {
	typedef std::vector<std::pair<TLorentzVector,double> > IHits;
	double PMAX=2.9135*irene::MeV;
	double EPS = 1e-3;

	const irene::Particle* SelectBetaMax(irene::Event* ievt);
	bool SelectBetaP(const irene::Particle* beta);
	void LogParticle(const irene::Particle* beta);
	IHits IreneHitVector(irene::Event* ievt);
	void LogHits(const IHits& ihits);
	void DrawHits(const IHits& ihits);
	IHits SparseHits(const IHits& ihits, int step);
	IHits SmearHits(const IHits& ihits, double sigma);
	

}
#endif