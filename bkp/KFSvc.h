#ifndef KFSVC_
#define KFSVC_
/*
 KFSvc: Manages Kalman Filter
 

 JJGC, April, 2014.
*/

#include <recpack/RecPackManager.h>
#include <recpack/string_tools.h>
#include <recpack/stc_tools.h>
#include <recpack/ERandom.h>
#include <recpack/HelixEquation.h>
#include <recpack/LogSpiralEquation.h>
#include <recpack/ParticleState.h>
#include <recpack/RayTool.h>
#include <recpack/EGeo.h>
#include <recpack/Definitions.h>
#include <recpack/Trajectory.h>
#include <recpack/KalmanFitter.h>
#include <string>
#include <vector>
#include <utility>
#include <memory>



namespace alex {

  class KFSetup;
  class ISvc;

class KFSvc {
	
  public:
		
    KFSvc(const KFSetup& kfs, const ISvc& isvc);
		virtual ~KFSvc(){};
		
    RP::Trajectory CreateTrajectory() ;
    std::string RPModel();
    int RPModelDim();
    void SetRPVerbosity();
    void CreateKFSetup();
 
    //std::string info[8] =  {"MUTE","ERROR","WARNING","INFO","NORMAL","DETAILED","VERBOSE","VVERBOSE"};
    
    // std::unique_ptr<RP::Trajectory>   Fit(const MeasCol& mc, 
    //                                       const KFSeedState& ssTrue, 
    //                                       const KFSeedState& ssGuess) ;

    // bool   Fit(const MeasCol& mc, 
    //            const KFSeedState& ssTrue, 
    //            const KFSeedState& ssGuess) ;

    // void Navigate(const MeasCol& mc);
    
    // RP::Trajectory FittedTrajectory() const {return fTraj;}
			
	protected:
    int RPModelDim();
    std::string RPModel();

		RP::RecPackManager fRPMan;   //Recpack magnager
  	RP::Setup fSetup;                // RP setup

  	const KFSetup& fKfs;         // pointer to setup
    const ISvc& fIsvc;           // pointer to Irene service
   //  KFSeedState* fSTrue;   // pointer to seed true
   //  KFSeedState* fSGuess;   // pointer to seed guess
   //  MeasCol* fMC;           // pointer to measurement

    // bool fBKfs;            //true first time we create a fKfs pointer
    // bool fBSet;

  	std::string fModel;         // set to helix by default
  	int fDim;                   // set to helix (6) by default

    double fX0;                // radiation length for this setup
    double fDedx;              // dedx for this setup

  // volume and surface properties cannot be local variables 
  // because RecPack takes a reference to the property

  	RP::EVector fBField;         // (Bx,By,Bz)
  	RP::EMatrix fCov;
  
  	RP::measurement_vector fMeas;
  	RP::Trajectory fSimTraj;
    RP::Trajectory fTraj;
			
	};
}
#endif