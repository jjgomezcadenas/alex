#ifndef RPUTIL_
#define RPUTIL_
/*
 RecPackUtil: RecPack Utilities 
 JJGC, July, 2014.
*/

#include <recpack/RecPackManager.h>
#include <recpack/string_tools.h>
#include <recpack/stc_tools.h>
#include <recpack/ERandom.h>
#include <recpack/HelixEquation.h>
#include <recpack/LogSpiralEquation.h>
#include <recpack/ParticleState.h>
#include <recpack/RayTool.h>
#include <recpack/EGeo.h>
#include <recpack/Definitions.h>
#include <recpack/Trajectory.h>
#include <recpack/KalmanFitter.h>
#include <string>

 
namespace alex {
	class KFSetup;
	void InitRecPackManager(const KFSetup& kfs, RP::RecPackManager& rp) ;
	std::string RPModel(const KFSetup& kfs);
	int RPModelDim(const KFSetup& kfs);
	void SetRPVerbosity(const KFSetup& kfs, RP::RecPackManager& rpMan);
	void CreateKFSetup(const KFSetup& kfs, RP::RecPackManager& rpMan);
	

}
#endif