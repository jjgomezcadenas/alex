#include "RecPackUtil.h"
#include <alex/KFSetup.h>
#include <CLHEP/Units/SystemOfUnits.h>
#include <alex/IreneUtil.h>

using std::string;
using std::endl; 
using std::cout;
using std::cin;
using std::vector;
using std::pair; 

using namespace Recpack;
using namespace CLHEP;
namespace alex {

  bool next::create_trajectory(const KFSetup& kfs, 
    IHits sphits, IHits smhits, double sigma, Trajectory& traj,
    const irene::Particle* part) 
  {

    // destroy measurements in _meas container
    stc_tools::destroy(_meas);
  
    //reset the trajectory
    _sim_traj.reset();
  
    EVector vv(7,0);
    EMatrix M(3,3,1);
    EVector m(3,0);
    EMatrix meas_cov(3,3,0);
    meas_cov[0][0] = meas_cov[1][1] = sigma;
    meas_cov[2][2] = 0.1*mm; //fake error, no smearing on z

    auto size = sphits.size();

    if (size != smhits.size())
    {
      std::cout << "ERROR: sparse and smeared hits do not have the same size" << std::endl;
      std::cout << "sphits.size() =" << sphits.size() << std::endl;
      std::cout << "smhits.size() =" << smhits.size() << std::endl;
      exit (EXIT_FAILURE);
    }

    for (size_t i=0; i<size; ++i)
    {
      auto sphit =sphits.at(i);
      auto smhit =smhits.at(i); 
      auto xyztSp = sphit.first;
      auto xyztSm = smhit.first;

      m[0]=xyztSp.X();
      m[1]=xyztSp.Y();
      m[2]=xyztSp.Z();

      smear_m[0]=xyztSm.X();
      smear_m[1]=xyztSm.Y();
      smear_m[2]=xyztSm.Z();
    
      string type=RP::xyz;
      
      // create a measurement
      Measurement* meas = new Measurement();    
      meas->set_name(type);                         // the measurement type
      meas->set_hv(HyperVector(smear_m,meas_cov,type));  // the HyperVector 
      meas->set_name(RP::setup_volume,"mother");          // the volume
        
      // the true position of the measurement
      meas->set_position_hv(HyperVector(m,EMatrix(3,3,0),RP::xyz));

    //    Add the measurement to the vector
    _meas.push_back(meas);
    
    
    // ------ Store true information for pulls -----------
    State state;
    TLorentzVector pos = part->GetInitialVertex();
    TLorentzVector mom = part->GetInitialMomentum();
    double q = part->GetCharge();

    int dim = RPModelDim(kfs);  

    EVector v(_dim,0);
    for (int k=0;k<3;k++)
      v[k]=pos[k];
    
    v[3]=mom[0]/mom[2];
    v[4]=mom[1]/mom[2];
    
    if (kfs.Model() == "helix") v[5]=q/(mom.Vect().Mag());
    
    state.set_hv(HyperVector(v,EMatrix(dim,dim,0),RP::slopes_curv_z));
    
    // create the node
    Node* node = new Node();
    node->set_measurement(*meas);
    node->set_state(state);
    _sim_traj.nodes().push_back(node);    
  }
    
  // add the simulated measurements to the trajectory
  traj.add_constituents(_meas);  
  
  return true;
}

  int RPModelDim(const KFSetup& kfs)
  {
    int dim;
    if (kfs.Model() =="helix")
    {
      dim = 6;                     // dimension of the state vector for this model
    } 
    else if (kfs.Model() =="sline")
    {
      dim = 5; 
    }
    else
    {
      std::cout << "ERROR: model unknown" << std::endl;
      exit (EXIT_FAILURE);
    }
    return dim;
  }

  string RPModel(const KFSetup& kfs)
  {
    string model;
    if (kfs.Model() =="helix")
    {
      model = RP::particle_helix;         // model used for reconstruction                   
    } 
    else if (kfs.Model() =="sline")
    {
      model = RP::particle_sline;         // model used for reconstruction
    }
    else
    {
      std::cout << "ERROR: model unknown" << std::endl;
      exit (EXIT_FAILURE);
    }
    return model;
  }

  void InitRecPackManager(const KFSetup& kfs, RP::RecPackManager& rpMan) 
  {
    log4cpp::Category& klog = log4cpp::Category::getRoot(); 
    string model = RPModel(kfs);
    

    klog << log4cpp::Priority::INFO 
        << "--InitRecPackManager: model = " << model;
    
    klog << log4cpp::Priority::DEBUG
        << "Select the model";
    rpMan.model_svc().select_model(model);

    klog << log4cpp::Priority::DEBUG
        << "Select the fitter";
    rpMan.fitting_svc().select_fitter(RP::kalman);

    klog << log4cpp::Priority::DEBUG
        << "Select the default representation";
    RP::rep().set_default_rep_name(RP::pos_dir_curv);

    klog << log4cpp::Priority::DEBUG
        << "Select the fitting representation";
    rpMan.fitting_svc().set_fitting_representation(RP::slopes_curv_z);

    klog << log4cpp::Priority::DEBUG
        << "Select the max chi2";

    rpMan.fitting_svc().retrieve_fitter<KalmanFitter>(RP::kalman,model).
    set_max_local_chi2ndf(kfs.MaxChi2());

    klog << log4cpp::Priority::DEBUG
        << "Select the max number of outliers";    
    rpMan.fitting_svc().retrieve_fitter<KalmanFitter>(RP::kalman,model).
    set_number_allowed_outliers(kfs.MaxOutliers());

    klog << log4cpp::Priority::DEBUG
        << "Select the max number of extrap failiures";  
    rpMan.fitting_svc().retrieve_fitter<KalmanFitter>(RP::kalman,model).
    set_max_consecutive_extrap_failures(kfs.MaxExtrapFailures());


    klog << log4cpp::Priority::DEBUG
        << "Init geometrical limits";
    rpMan.geometry_svc().set_zero_length(1e-3 * mm);
    rpMan.geometry_svc().set_infinite_length(1e12 * mm);

    // minimum distance between nodes to check the node ordering. 
    // Specially  for P0D which can have clusters at the same layer with different z positions
    rpMan.matching_svc().set_min_distance_node_ordering(kfs.MinDistanceNodeOrdering());

    // minimum number of nodes to be check for good ordering
    rpMan.matching_svc().set_min_good_node_ordering(kfs.MinGoodNodeOrdering());

    klog << log4cpp::Priority::DEBUG
        << "Enable MS";
    rpMan.model_svc().enable_noiser(model, RP::ms, true);

    // disable energy loss fluctuations by default
    rpMan.model_svc().enable_noiser(model, RP::eloss, false);

    // disable electron energy loss fluctuations (bremsstrahlung) by default
    rpMan.model_svc().enable_noiser(model, RP::electron_eloss, false);

    // disable electron energy loss correction (bremsstrahlung) by default
    rpMan.model_svc().enable_correction(model, RP::brem_eloss, false);

    // enable energy loss correction by default
    rpMan.model_svc().enable_correction(model, RP::eloss, true);

    // By default no preselected length sign is used when intersecting a surface
    rpMan.model_svc().model(model).intersector().set_length_sign(0);

    // The geometry is not initialized for this manager
    rpMan.geometry_svc().set_status("ready",false);

    
    klog << log4cpp::Priority::DEBUG
        << "Create the setup";
    CreateKFSetup(kfs,rpMan);

   klog << log4cpp::Priority::DEBUG
        << "Setup Created";
    SetRPVerbosity(kfs,rpMan);

    
  }

  void CreateKFSetup(const KFSetup& kfs, RP::RecPackManager& rpMan)
  {
    log4cpp::Category& klog = log4cpp::Category::getRoot();
    Setup setup;  //RP Setup
    string model = RPModel(kfs);

    klog << log4cpp::Priority::DEBUG
        << "Create CreateKFSetup";

    auto BField = EVector(3,0);
    BField[2]=kfs.B();

    klog << log4cpp::Priority::DEBUG
        << "BField -->" << BField[2]/tesla;

    // box dimensions
    double S = 100*m;

    // the axes for the definition of volumes and surfaces
    EVector xaxis(3,0);
    EVector yaxis(3,0);
    EVector zaxis(3,0);

    xaxis[0] =1.; yaxis[1]=1.; zaxis[2] = 1.; 

    // 1. Give a name to the setup
    setup.set_name("main");

    // 2.  Create a mandatory mother volume

    klog << log4cpp::Priority::DEBUG
        << "Create mother volume";

    EVector pos(3,0);
    pos[0]=0.; pos[1]=0; pos[2]=0;  // at (x,y,z)=(0,0,0)
    Volume* mother = new Box(pos,xaxis,yaxis,S,S,S);
    setup.set_mother(mother);

    // 3. Define its properties

    klog << log4cpp::Priority::DEBUG
        << "Use surfaces normal to Z";
    setup.set_volume_property("mother",RP::SurfNormal,zaxis);

    if (kfs.Model() =="helix") 
      setup.set_volume_property("mother",RP::BField,BField);

    if (kfs.X0() != 0) 
    {
      double x0 = kfs.X0();   
      setup.set_volume_property("mother",RP::X0,x0); 
    }

    if (kfs.DEDX() != 0.) 
    {
      double dedx = kfs.DEDX(); 
      setup.set_volume_property("mother",RP::de_dx,dedx);  
    }

    // 4. add the setup to the geometry service
    klog << log4cpp::Priority::DEBUG
        << "Add the setup to the geometry service";
    rpMan.geometry_svc().add_setup("main",setup);

    // 5. select the setup to be used by the geometry service
    rpMan.geometry_svc().select_setup("main");

     // navigation strategy (the navigator needs the setup)
    klog << log4cpp::Priority::DEBUG
        << "Navigation strategy";
    rpMan.navigation_svc().navigator(model).set_unique_surface(true);

    // The geometry is initialized for this manager
    rpMan.geometry_svc().set_status("ready",true);


    klog << log4cpp::Priority::INFO 
        << "Recpack Setup: " << setup;
  
  klog << log4cpp::Priority::INFO 
        << "Dump completed: " ;
  }

  void SetRPVerbosity(const KFSetup& kfs, RP::RecPackManager& rpMan) 
  {
    //{"MUTE","ERROR","WARNING","INFO","NORMAL","DETAILED","VERBOSE","VVERBOSE"};

    log4cpp::Category& klog = log4cpp::Category::getRoot();

    vector<string> vb =kfs.FiNaMoMaVerbosity();  //FitterNavigationModelMatching
    Messenger::Level level[4];

    for(int i=0; i <4; ++i)
    {
      level[i] =Messenger::str(vb.at(i));

      klog << log4cpp::Priority::DEBUG 
        << " In SetRPVerbosity: \n  " 
        << " level: " << i << " verbosity = " << vb.at(i) <<  "\n";
    }

    string model = RPModel(kfs);

    klog << log4cpp::Priority::DEBUG 
        << " In SetRPVerbosity: \n  " 
        << " level0: " << level[0] << "\n"
        << " level1: " << level[1] << "\n"
        << " level2: " << level[2] << "\n"
        << " level3: " << level[3] << "\n";
        

    Messenger::Level level0 = level[0];
    Messenger::Level level1 = level[1];
    Messenger::Level level2 = level[2];
    Messenger::Level level3 = level[3];
    Messenger::Level level4 = level[3];

    const std::string& modelname = rpMan.model_svc().model_name();

    klog << log4cpp::Priority::DEBUG 
        << " model name = " << modelname ;
        
    klog << log4cpp::Priority::DEBUG 
        << " set verbosity for fitting: level0 = " << level0 ;

    rpMan.fitting_svc().fitter(modelname).set_verbosity(level0);

    klog << log4cpp::Priority::DEBUG 
        << " set verbosity for navigation: level1 = " << level1 ;

    rpMan.navigation_svc().set_verbosity(level1);
    rpMan.navigation_svc().navigator(modelname).set_verbosity(level1);
    rpMan.navigation_svc().inspector(RP::X0).set_verbosity(level1);
    rpMan.navigation_svc().navigator(modelname).master_inspector().set_verbosity(level1);
    rpMan.navigation_svc().inspector(RP::BField).set_verbosity(level1);  
    rpMan.navigation_svc().inspector(RP::eloss).set_verbosity(level1);     
    rpMan.navigation_svc().inspector(RP::Nel).set_verbosity(level1);     
    rpMan.navigation_svc().inspector(RP::elossMap).set_verbosity(level1);     
    rpMan.navigation_svc().inspector(RP::BFieldMap).set_verbosity(level1);     
    rpMan.model_svc().model(modelname).intersector("plane").set_verbosity(level1);
    rpMan.model_svc().model(modelname).intersector("numerical").set_verbosity(level1);

    klog << log4cpp::Priority::DEBUG 
        << " set verbosity for model: level2 = " << level2;

    rpMan.model_svc().model(modelname).equation().set_verbosity(level2);
    rpMan.model_svc().model(modelname).propagator().set_verbosity(level2);
    rpMan.model_svc().model(modelname).noiser().set_verbosity(level2);
    rpMan.model_svc().model(modelname).tool("correction/eloss").set_verbosity(level2);
    rpMan.model_svc().model(modelname).tool("correction/brem_eloss").set_verbosity(level2);
    rpMan.model_svc().model(modelname).tool("noiser/brem_eloss").set_verbosity(level2);
    rpMan.model_svc().model(modelname).tool("noiser/ms").set_verbosity(level2);
    rpMan.model_svc().model(modelname).tool("noiser/eloss").set_verbosity(level2);
    rpMan.model_svc().model(modelname).tool("intersector/numerical").set_verbosity(level2);

    klog << log4cpp::Priority::DEBUG 
        << " set verbosity for matching: level3 = " << level3;

    rpMan.conversion_svc().projector().set_verbosity(level2);
    rpMan.conversion_svc().representation(RP::slopes_curv).set_verbosity(level2);

    rpMan.matching_svc().set_verbosity(level3);
  //rpMan.matching_svc().surface_maker("global").set_verbosity(level3);

    RayTool::msg().set_verbosity(level4);

}

  

}