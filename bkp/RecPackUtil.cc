#include "RecPackUtil.h"
#include <alex/KFSetup.h>
#include <alex/LogUtil.h>

using std::string;
using std::endl; 
using std::cout;
using std::cin;
using std::vector;
using std::pair; 

using namespace Recpack;
namespace alex {

  int RPModelDim(const KFSetup& kfs)
  {
    int dim;
    if (kfs.Model() =="helix")
    {
      dim = 6;                     // dimension of the state vector for this model
    } 
    else if (kfs.Model() =="sline")
    {
      dim = 5; 
    }
    else
    {
      std::cout << "ERROR: model unknown" << std::endl;
      exit (EXIT_FAILURE);
    }
    return dim;
  }
  string RPModel(const KFSetup& kfs)
  {
    string model;
    if (kfs.Model() =="helix")
    {
      model = RP::particle_helix;         // model used for reconstruction                   
    } 
    else if (kfs.Model() =="sline")
    {
      model = RP::particle_sline;         // model used for reconstruction
    }
    else
    {
      std::cout << "ERROR: model unknown" << std::endl;
      exit (EXIT_FAILURE);
    }
    return model;

  }

  void SetRPVerbosity(const KFSetup& kfs) 
  {

    //{"MUTE","ERROR","WARNING","INFO","NORMAL","DETAILED","VERBOSE","VVERBOSE"};

    RP::RecPackManager rpMan;

    vector<string> vb =afs.FiNaMoMaVerbosity();  //FitterNavigationModelMatching
    Messenger::Level level[4];

    for(int i=0; i <4; ++i)
    {
      level[i] =Messenger::str(vb.at(i));
    }

    string model = RPModel(kfs);
    
    // verbosity levels related with fitting
    rpMan.fitting_svc().fitter(model).set_verbosity(level[0]);

    // verbosity levels related with navigation
    rpMan.navigation_svc().set_verbosity(level[1]);
    rpMan.navigation_svc().navigator(model).set_verbosity(level[1]);
    rpMan.navigation_svc().inspector("ms").set_verbosity(level[1]);
    rpMan.navigation_svc().navigator(model).master_inspector().set_verbosity(level[1]);
    rpMan.navigation_svc().inspector("BField").set_verbosity(level[1]);      
    rpMan.model_svc().model(model).intersector("plane").set_verbosity(level[1]);
    rpMan.conversion_svc().surface_projector().set_verbosity(level[1]);


    if (model==RP::particle_helix || model==RP::helix)
    { 
      rpMan.model_svc().model(model).intersector("numerical").set_verbosity(level[1]);
      rpMan.model_svc().model(model).intersector("helix_num").set_verbosity(level[1]);
    }

    // verbosity levels related with model operation (soft intersection)
    rpMan.model_svc().model(model).equation().set_verbosity(level[2]);
    rpMan.model_svc().model(model).propagator().set_verbosity(level[2]);
    rpMan.model_svc().model(model).noiser().set_verbosity(level[2]);
    rpMan.model_svc().model(model).tool("correction/eloss").set_verbosity(level[2]);
    rpMan.model_svc().model(model).tool("noiser/ms").set_verbosity(level[2]);

    // verbosity levels related with matching
    rpMan.matching_svc().set_verbosity(level[3]);
  }


  InitRecPack(const KFSetup& kfs) 
  {
    RP::RecPackManager rpMan;
    log4cpp::Category& klog = log4cpp::Category::getRoot();

    string model = RPModel(kfs);
    int dim = RPModelDim(kfs);
    
    // select the model
    rpMan.model_svc().select_model(model);

    // initialize geometrical limits
    rpMan.geometry_svc().set_zero_length(1e-5 * mm);
    rpMan.geometry_svc().set_infinite_length(1e12 * mm);

    // select the fitter
    rpMan.fitting_svc().select_fitter(RP::kalman);

    // and the fitting representation
    rpMan.fitting_svc().set_fitting_representation(RP::slopes_curv_z);

    // radiation length (default is 0*cm)
 
    if (kfs.X0() == 0){
      rpMan.model_svc().enable_noiser(model, RP::ms, false);
    }

    //energy loss rate
  
    if (kfs.DEDX() == 0){
      rpMan.model_svc().enable_correction(model, RP::eloss, false);
    }

    // create the experimental setup
    CreateKFSetup(kfs);

    // navigation strategy (the navigator needs the setup)
    rpMan.navigation_svc().navigator(model).set_unique_surface(true);

    klog << log4cpp::Priority::INFO 
        << "Recpack Setup: " << setup;
  
  }

  void CreateKFSetup(const KFSetup& kfs)
  {
    Setup setup;  //RP Setup
    auto BField = EVector(3,0);
    BField[2]=kfs.B();

    // box dimensions
    double S = 100*m;

    // the axes for the definition of volumes and surfaces
    EVector xaxis(3,0);
    EVector yaxis(3,0);
    EVector zaxis(3,0);

    xaxis[0] =1.; yaxis[1]=1.; zaxis[2] = 1.; 

    // 1. Give a name to the setup
    setup.set_name("main");

    // 2.  Create a mandatory mother volume
    EVector pos(3,0);
    pos[0]=0.; pos[1]=0; pos[2]=0;  // at (x,y,z)=(0,0,0)
    Volume* mother = new Box(pos,xaxis,yaxis,S,S,S);
    setup.set_mother(mother);

    // 3. Define its properties

    // Use surfaces normal to Z
    setup.set_volume_property("mother",RP::SurfNormal,zaxis);

    if (kfs.Model() =="helix") 
      setup.set_volume_property("mother",RP::BField,BField);

    if (kfs.X0() != 0)    
      setup.set_volume_property("mother",RP::X0,kfs.X0()); 

    if (kfs.DEDX() != 0.) 
      setup.set_volume_property("mother",RP::de_dx,kfs.DEDX());  

    // 4. add the setup to the geometry service
    rpMan.geometry_svc().add_setup("main",setup);

    // 5. select the setup to be used by the geometry service
    rpMan.geometry_svc().select_setup("main");

    // dump the setup
    std::cout << setup << std::endl;
  
  }

//*************************************************************
void next::compute_seed_state(const irene::Particle& part, const Trajectory& traj, State& state) {
//*************************************************************

  EVector v(_dim,0);
  EMatrix C(_dim,_dim,0);

  const RecObject& meas = traj.nodes()[0]->measurement();
  const EVector& m = meas.vector();

  TLorentzVector ul = part.GetInitialMomentum();
  EVector u(3,0);
  u[0] = ul.X();
  u[1] = ul.Y();
  u[2] = ul.Z();

  v[0] = m[0];   // a guess of the x position
  v[1] = m[1];   // a guess of the y position
  v[2] = m[2];  // a guess of the z position  
  v[3] = u[0]/u[2];  // a guess of dx/dz (use truth for the moment)
  v[4] = u[1]/u[2];  // a guess of dy/dz (use truth for the moment)

  if (mhelix)
    // // a guess of q/p. If unknown give large momentum 
    //    v[5]=-1/(part.Momentum());
    v[5]=-1/(10.);
  else{
    // For the Straight line model q/p is a fix parameter use for MS computation 
    double qoverp=1/(1*GeV);
    state.set_hv(RP::qoverp,HyperVector(qoverp,0));
  }

  // give a large diagonal covariance matrix
  C[0][0]= C[1][1]=100.*cm;
  C[2][2]= EGeo::zero_cov()/2.; // no error on Z since planes are perpendicular to z
  C[3][3]= C[4][4]=1;

  if (mhelix)
    C[5][5]= 0.7;
  
  // Set the main HyperVector
  state.set_hv(HyperVector(v,C));

  // Set the sense HyperVector (1=increasing z)
  double sense=u[2]/fabs(u[2]); // use truth for the moment
  state.set_hv(RP::sense,HyperVector(sense,0));

  // Set the model name
  state.set_name(_model);

  // Set the representation (x,y,z dx/dz, dy/dz, q/p)
  if (mhelix)
    state.set_name(RP::representation,RP::slopes_curv_z);
  else
    state.set_name(RP::representation,RP::slopes_z);

}

//*************************************************************
bool next::execute() {
//*************************************************************

  irene::Event* ievt = new irene::Event();
  _evtTree->SetBranchAddress("EventBranch", &ievt);

  int nentries = _evtTree->GetEntries();
  int nmax = std::min(_nevents,nentries);

  for (int evt=0;evt<nmax;evt++){

    if (evt%100 == 0) std::cout << "event: "<< evt << std::endl; 

    int nb = _evtTree->GetEntry(evt);
    (void)nb;

    for (int i=0;i<ievt->GetTracks()->GetEntriesFast(); i++){
      irene::Track * track =(irene::Track*)(*(ievt->GetTracks()))[i];  

      if (fabs(track->GetParticle()->Momentum()-2.9*MeV)>0.1) continue;
      
      // create the trajectory. Read it from a text file
      Trajectory traj;
      bool ok = create_trajectory(*track, traj);
      if (!ok) return ok;

      //      std::cout << track->GetParticle()->Momentum() << " " << track->GetParticle()->Energy() << " " << track->GetParticle()->GetCharge() << std::endl;

      // compute the seed state ( for Kalman Filter fitting)  
      State seed;
      compute_seed_state(*(track->GetParticle()), traj, seed);
      
      // fit the trajectory provided a seed state
      ok = rpMan.fitting_svc().fit(seed,traj);
      if (!ok) return ok;

      // compute the pulls
      //      compute_pulls(_sim_traj,traj);

      fill_histos(traj);
      
    }
  }

  /*
  //  example of extrapolation
  example_of_propagation(traj);

  // example of matching
  example_of_matching(traj);
    */

  return true;
}

//*************************************************************
void next::example_of_propagation(Trajectory& traj) {
//*************************************************************

  // 1. Create a surface at (0,0,10000)
  EVector pos(3,0); pos[2]=10000*mm;
  EVector normal(3,0); normal[2]=1;
  Plane surf(pos,normal);  // Can also be Rectangle, Ring, Cylinder  (see recpack/geometry)

  // 2. add it temporarily to the mother volume in the setup setup
  std::string volname = "mother";
  std::string surfname = "test-surface";
  rpMan.geometry_svc().setup().add_surface(volname,surfname, &surf);

  // 3. find the closest state of the trajectory to de surface 
  double length;
  int istate;
  bool ok = rpMan.navigation_svc().closest_state(surf, traj, istate, length);
  State pstate = traj.node(istate).state();
  std::cout << "propagation: initial state = "<< print( pstate.vector() ) << std::endl;

  // 4. propagate that state to the surface
  ok = rpMan.navigation_svc().propagate(surf, pstate, length);
  if (ok)
    std::cout << "   ---> length: " << length << " propagated vector: " << print( pstate.vector() ) << std::endl;
  else
    std::cout << "   ---> propagation failed !!!" << std::endl;

  // 5. remove the temporary surface
  rpMan.geometry_svc().setup().remove_surface(surfname);


  // ----------------------

  // Another example of propagation. This time to a given length.

  length = -100*cm;
  pstate = traj.node(istate).state();
  ok = rpMan.navigation_svc().propagate(length, pstate);
  if (ok)
    std::cout << "propagate to length ---> propagated vector = " << print( pstate.vector() ) << std::endl;
  else
    std::cout << "propagate to length ---> propagation failed !!!" << std::endl;



}

//*************************************************************
void next::example_of_matching(Trajectory& traj) {
//*************************************************************

  // 1. Retrieve the first measurement in the trajectory
  const Measurement& meas = traj.node(0).measurement();

  // 2. Match the trajectory to that measurement
  double chi2ndf;
  bool ok = rpMan.matching_svc().match_trajectory_measurement(traj, meas, chi2ndf);
  if (ok)
    std::cout << "matching:  chi2/ndf = " << chi2ndf << std::endl;
  else
    std::cout << "matching:  failed !!!! = " << chi2ndf << std::endl;

  // have a look ar recpack/manager/MatchingSvc.cpp to see all available matching functions

}

//*************************************************************
bool next::create_trajectory(irene::Track& track, Trajectory& traj) {
//*************************************************************

  // destroy measurements in _meas container
  stc_tools::destroy(_meas);
  
  //reset the trajectory
  _sim_traj.reset();
  
  EVector vv(7,0);
  EMatrix M(3,3,1);
  EVector m(3,0);

  for (unsigned int i=0;i<track.GetHits().size();i++){

    // add to the RecPack trajectory one hit every ten
    if (i%10!=0) continue; 

    TVector3 vh= track.GetHits()[i].first.Vect();

    m[0]=vh[0]*mm;
    m[1]=vh[1]*mm;
    m[2]=vh[2]*mm;
    
    std::string type=RP::xyz;
    EMatrix meas_cov(3,3,0);
    meas_cov[0][0] = meas_cov[1][1] = meas_cov[2][2] = 1*mm;
    
    // smear the true hits
    EVector smear_m = ERandom::gaussian(m,meas_cov);
    
    // ------ THIS IS THE IMPORTANT PART (Example of measurement creation) -----------
    
    // create a measurement
    Measurement* meas = new Measurement();    
    meas->set_name(type);                         // the measurement type
    meas->set_hv(HyperVector(smear_m,meas_cov,type));  // the HyperVector 
    meas->set_name(RP::setup_volume,"mother");          // the volume
        
    // the position of the measurement
    meas->set_position_hv(HyperVector(m,EMatrix(3,3,0),RP::xyz));

    //    Add the measurement to the vector
    _meas.push_back(meas);
    
    
    // ------ END OF THE IMPORTANT PART -----------

    irene::Particle *part = track.GetParticle();    
    TLorentzVector pos = part->GetInitialVertex();
    TLorentzVector mom = part->GetInitialMomentum();
    double q = part->GetCharge();

    // creates a state (true information for pulls)
    State state;
    EVector v(_dim,0);
    for (int k=0;k<3;k++)
      v[k]=pos[k];
    
    v[3]=mom[0]/mom[2];
    v[4]=mom[1]/mom[2];
    
    if (mhelix) v[5]=q/(mom.Vect().Mag());
    
    state.set_hv(HyperVector(v,EMatrix(_dim,_dim,0),RP::slopes_curv_z));
    
    // create the node
    Node* node = new Node();
    node->set_measurement(*meas);
    node->set_state(state);
    _sim_traj.nodes().push_back(node);    
  }
    
  // add the simulated measurements to the trajectory
  traj.add_constituents(_meas);  
  
  return true;
}

//*************************************************************
bool next::finalize() {
//*************************************************************

//  dump_pulls();

  _ntuple->Write();
  _outputFile->Close();

  return true;
}


//*************************************************************
void next::fill_histos(const Trajectory& traj) {
//*************************************************************
  
  const EVector& v = traj.state(0).vector();
  const EMatrix& C = traj.state(0).matrix();

  double p=0;
  double charge=0;
  if (v[5]!=0){
    p = 1/fabs(v[5]);
    charge = v[5]/fabs(v[5]);
  }
  double qop = v[5];
  double eqop = sqrt(C[5][5]);

  _ntuple->Fill(traj.quality("chi2"),traj.ndof(),charge,p,qop,eqop);
  
}

