#include "IreneUtil.h"
#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TCanvas.h>
#include <TStyle.h>
#include <TRandom.h>
#include <iostream>
#include <string>
#include <sstream>


using std::string;
using std::endl; 
using std::cout;
using std::cin;
using std::vector;
using std::pair; 


namespace alex {
  
  IHits IreneHitVector(irene::Event* ievt)
  {
    IHits ihits;
    ievt->FillHitVector(ihits, "ACTIVE");
    return ihits;
  }
 
  const irene::Particle* SelectBetaMax(irene::Event* ievt)
  {
    const irene::Particle* betaMax;
    double emax =0;

    for (size_t it = 0; it < ievt->Tracks().size(); ++it)
    {   
      const irene::Particle* ip= ievt->Tracks().at(it)->GetParticle();
    
      if (ip->GetPDGcode() != 11) // not interested if not an electron.
        continue;

      if (ip->Energy() > emax)
      {
        emax = ip->Energy();
        betaMax = ip;
      }
    } 
    return betaMax;
  }

  bool SelectBetaP(const irene::Particle* beta)
  {
    if (abs(beta->Momentum() -PMAX) < EPS)
      return true;
    else
      return false;
  }

  void LogParticle(const irene::Particle* beta)
  {
    TLorentzVector pi = beta->GetInitialMomentum();
    TLorentzVector vi = beta->GetInitialVertex();
    TLorentzVector vf = beta->GetDecayVertex();

    cout << " pi (px,py,pz) MeV: =  " << pi[0] << "," << pi[1]<< "," << pi[2] << endl;
    cout << " vi (x,y,z)  mm:    =  " << vi[0] << "," << vi[1]<< "," << vi[2] << endl;
    cout << " vf (x,y,z)  mm:    =  " << vf[0] << "," << vf[1]<< "," << vf[2] << endl;  
    cout  << " P (MeV)=  " << beta->Momentum()
        << " E (MeV)  = "<< beta->Energy() 
        << " T (MeV)= " << beta->Energy()-beta->Momentum()<< endl;
  }

  void LogHits(const IHits& ihits)
  {
    for (auto it=ihits.begin(); it!=ihits.end(); ++it)
    {
    	auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

      printf(" hit: x= %7.3f y= %7.3f z=%7.3f (mm) e = %7.3f (keV) \n", 
            xyzt.X()/irene::mm, xyzt.Y()/irene::mm, 
            xyzt.Z()/irene::mm,energy/irene::keV); 
    }
  }

  IHits SparseHits(const IHits& ihits, int step)
  {
  	IHits spHits;
  	int i=0;
  	double edep =0;
    for (auto it=ihits.begin(); it!=ihits.end(); ++it)
    {
    	edep += it->second;
    	if (i==0 or i == step)
    	{
    		auto hit = *it;
      	pair<TLorentzVector,double> nhit;
      	nhit.first = it->first;
      	nhit.second = edep;
      	spHits.push_back(nhit);
    		
    		i=0;
    		edep=0;
    	}
    	i++;
    }
    return spHits;
  }

  IHits SmearHits(const IHits& ihits, double sigma)
  {
  	IHits smHits;
    for (auto it=ihits.begin(); it!=ihits.end(); ++it)
    {
      auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

      TLorentzVector sxyz;
      for (int i=0; i<2; ++i)
      {
      	auto dx = gRandom->Gaus(0,1)*sigma;
      	auto sx = gRandom->Rndm();
      	auto sgnx = 1;
      	if (sx >0.5) sgnx =-1;
      	sxyz[i] = xyzt[i]+sgnx*dx;
      }
      sxyz[2]=xyzt[2];
      sxyz[3]=xyzt[3];

      pair<TLorentzVector,double> nhit;
      nhit.first = sxyz;
      nhit.second = energy;
      smHits.push_back(nhit);
    }
    return smHits;
  }

  void DrawHits(const IHits& ihits)
  {
    TCanvas *c1 = new TCanvas( "c1", "Hits", 200, 10, 600, 800 );
    gStyle->SetOptStat(1000000001);

    auto nhit = ihits.size();
    // auto hit0 = *ihits.begin();
    // auto hitl = *ihits.end();
    // auto xl = hit0.first.X();
    // auto xu = hitl.first.X();
    // auto yl = hit0.first.Y();
    // auto yu = hitl.first.Y();
    // auto zl = hit0.first.Z();
    // auto zu = hitl.first.Z();
    auto zl = -200;
    auto zu = 200;
    auto xl = -200;
    auto xu = 200;
    auto yl = -200;
    auto yu = 200;

    auto hxz = new TH2F("hxz", "Hits: x vs z", nhit, zl, zu, nhit, xl, xu);
    auto hyz =  new TH2F("hyz", "Hits: y vs z", nhit, zl, zu, nhit, yl, yu);
    auto hxy =  new TH2F("hxy", "Hits: x vs y", nhit, xl, xu, nhit, yl, yu);

    for (auto it=ihits.begin(); it!=ihits.end(); ++it)
    {
      auto hit = *it;
      auto xyzt = hit.first;
      auto energy=hit.second;

       hxz->Fill(xyzt.Z(), xyzt.X(), energy);
       hyz->Fill(xyzt.Z(), xyzt.Y(), energy);
       hxy->Fill(xyzt.X(), xyzt.Y(), energy);

    }

    
    c1->Divide(1,3);
    
    c1->cd(1);
    hxz->Draw("colz");
    c1->cd(2);
    hyz->Draw("colz");
    c1->cd(3);
    hxy->Draw("colz");
    c1->Update();

    string input ;
    cout << "Return to continue:\n>";
    getline(cin, input);

    delete hxz;
    delete hyz;
    delete hxy;
    delete c1;
 

  }
}
    