#ifndef ACONFIG_
#define ACONFIG_
/*
 ACONFIG: Alex Configuration
 

 JJGC, July, 2014.
*/

#include <string>
#include <vector>
#include <utility>
#include <memory>
#include <iostream>
#include <map>
#include <sstream>
#include <alex/SingletonTemplate.h>


namespace alex {

class AlxConfig {
	public:
		AlxConfig(){};
		virtual ~AlxConfig(){};
		bool OpenXmlFile(std::string xmlFile);

		void AddInteger(std::string key, int value){fIntData[key]=value;}
		void AddDouble(std::string key, double value){fDoubleData[key]=value;}
		void AddString(std::string key, std::string value){fStringData[key]=value;}

		double GetDouble(std::string key){return fDoubleData[key];}
		int GetInteger(std::string key){return fIntData[key];}
		std::string GetString(std::string key){return fStringData[key];}

		std::string Info() const;


	private:
		std::vector<int>  GetIntArray(const char* n,const char* text) const ;
		std::map<std::string,std::string> fStringData;
		std::map<std::string,int> fIntData;
		std::map<std::string,double> fDoubleData;
		std::map<std::string,std::vector<int>> fIntArray;
			
			
	};

	typedef SingletonTemplate<AlxConfig> AConfigSvc;   // Global declaration

}
#endif