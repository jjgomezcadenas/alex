
// ----------------------------------------------------------------------------
//  $Id: AlxConfig.cc 
//
//  Author:  <gomez@mail.cern.ch>
//  Created: July 2014
//  
//  Copyright (c) 2014 NEXT Collaboration
// ---------------------------------------------------------------------------- 

#include <alex/AConfigSvc.h>
#include <alex/xmlParser.h>
#include <cstdlib>
#include <iostream>
#include <string>
#include <sstream>
#include <cstdio>
#include <cstring>

using std::string; 
using std::cout; 
using std::endl; 
using std::ostream;
using std::vector;


namespace alex {

  /* strtok example */


  //--------------------------------------------------------------------
  bool AlxConfig::OpenXmlFile(std::string xmlFile)
  //--------------------------------------------------------------------
  {

    // this open and parse the XML file:
    XMLNode xMainNode=XMLNode::openFileHelper(xmlFile.c_str(),"PMML");

    // this gets the number of "DataField" tags:

    auto xDataDict=xMainNode.getChildNode("DataDictionary");
    int ndf=xDataDict.nChildNode("DataField");

    printf ("number of data fields = %d\n",ndf );

    // this gets the number of "Array" tags:

    int nar=xDataDict.nChildNode("Array");

    printf ("number of arrays = %d\n",nar );

    int i,myIterator=0;

    for (i=0; i<nar; i++)
    {
      auto xDataField = xDataDict.getChildNode("Array",&myIterator);
      std::string  name = xDataField.getAttribute("name");
      const char* text =  xDataField.getText();
      const char* n = xDataField.getAttribute("n");
      std::string  type = xDataField.getAttribute("type");

      cout << "name of array = " << name << endl;
      cout << "type of array = " << type << endl;
      if (type == "integer")
      {
        vector<int> iar = GetIntArray(n,text);
        fIntArray[name]=iar;
      }
    }

    // this gets the number of "REAL-NUMBER" tags:

    int nrn=xDataDict.nChildNode("REAL-NUMBER");

    printf ("number of RN = %d\n",nrn );

    for (i=0; i<ndf; i++)
    {
        auto xDataField = xDataDict.getChildNode("DataField",&myIterator);
        auto name = xDataField.getAttribute("name");
        std::string dataType =  xDataField.getAttribute("dataType");
        auto xDataValue=xDataField.getChildNode("Value");
        auto value =  xDataValue.getAttribute("value");
        
        
        // std::cout << "name of data field =" << name 
        //           << " value = " << value 
        //           <<" dataType = " << dataType << std::endl;

        if (dataType=="double")
        {
          double dval = std::stod (value);
          //printf ("found a double: value = %7.1f\n", dval);
          fDoubleData[name]=dval;
        }
        else if (dataType=="integer")
        {
          int ival = std::stoi (value);
          //printf ("found an integer: value = %d\n", ival);
          fIntData[name]=ival;
        }

        else if (dataType=="string")
        {
          //std::cout << "found a string: value = "<< value << std::endl;
          fStringData[name]=value;
        }

        else
        {
          std::cout << "ERROR: data type unknown" << std::endl;
          exit (EXIT_FAILURE);
        }
    }

    return true;
  }
//--------------------------------------------------------------------
  string AlxConfig::Info() const
//--------------------------------------------------------------------
  {
    std::ostringstream s;
    s << " data stored in Service" << std::endl;    
    s << "Integer data:----" << std::endl;

    for (auto it=fIntData.begin(); it!=fIntData.end(); ++it)
    {
      s << "name = " << it->first << " value = " << it->second << std::endl;
    }

    s << "Double data:----" << std::endl;

    for (auto it=fDoubleData.begin(); it!=fDoubleData.end(); ++it)
    {
      s << "name = " << it->first << " value = " << it->second << std::endl;
    }

    s << "String data:----" << std::endl;

    for (auto it=fStringData.begin(); it!=fStringData.end(); ++it)
    {
      s << "name = " << it->first << " value = " << it->second << std::endl;
    }
    return s.str();
  }
//--------------------------------------------------------------------  
  vector<int>  AlxConfig::GetIntArray(const char* n,const char* text) const 
//--------------------------------------------------------------------
  {
    
    cout << "dimension of array = " << n << endl;
    cout << "text of array = " << text << endl;

    int dim = std::stoi (n);

    cout << "dimension of array (int) = " << dim << endl;
    char* value = new char[dim];
    strcpy ( value, text );

    vector<int> iar;

    char * pch;
    pch = strtok (value," ");

    // cout << "value of pch = " << pch << endl;
    // int ival = std::stoi (pch);

    // cout << "value of pch (int) = " << ival << endl;
    // iar.push_back(ival);
    
    while (pch != NULL)
    { 
      int ival = std::stoi (pch);
      iar.push_back(ival);
      pch = strtok (NULL, " ");
    }

    cout << "integer array = "  << endl;
    for (auto i : iar) 
        std::cout << i << ' ';
    cout << endl;

    return iar;

  }

}

