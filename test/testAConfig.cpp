// Test for AConfig
// Adapted for NEXT by J.J. Gomez-Cadenas, 2014


#include <alex/AConfigSvc.h>



int main(int argc, char **argv)
{
	alex::AConfigSvc::Instance().OpenXmlFile("alexConfig2.xml");

    std::cout << "printing configuration data" << std::endl;
    std::cout << alex::AConfigSvc::Instance().Info() << std::endl;
  
    return 0;
  }
  
  
