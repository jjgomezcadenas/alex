// Simple driver for Alex
// Adapted for NEXT by J.J. Gomez-Cadenas, 2014

//MagBox_Xe_10atm_00tesla.Xe136_bb0nu.0.next
//MagBox_Xe_10atm_00tesla.e2447.next

#include <TFile.h>
#include <TTree.h>
#include <TVector3.h>
#include <TApplication.h>
#include <TCanvas.h>

#include <TH1F.h>
#include <TH2F.h>
#include <TH3F.h>
#include <TStyle.h>

#include <stdio.h>
#include <string>     // std::string, std::stod
#include <vector>   
#include <algorithm>
#include <iostream>

#include <alex/xmlParser.h>
#include <alex/AConfig.h>
#include <alex/LogUtil.h>
#include <alex/GenUtil.h>
#include <alex/ISvc.h>
#include <alex/KFSetup.h>
#include <alex/KFSvc.h>

#include <paolina/Voxel.h>
#include <paolina/VoxelBuilder.h>
#include <paolina/Track.h>
#include <paolina/TrackBuilder.h>
#include <paolina/BlobBuilder.h>
#include <paolina/Blob.h>


using std::string; 
using std::cout;
using std::cin; 
using std::endl; 
using std::vector;
using std::pair; 

void DrawVoxels(std::vector<paolina::Voxel*> voxels, vector<double> hl, vector<double> hu) ;

using namespace alex;


int main(int argc, char **argv)
{
  // to display histos online
  TApplication* theApp = new TApplication("App", &argc, argv);


  //logger
  InitLogger();
  log4cpp::Category& klog = log4cpp::Category::getRoot();

  //configuration
  auto acf = AConfig("alexConfig.xml");

  cout << "printing configuration data" << endl;
  cout << acf << endl;

  // read data from xml file----------

  //data file path
  string dataPath = acf.GetString("dataPath");
  string fileName = acf.GetString("fileName");

  //debugging flags
  string debugLevel = acf.GetString("debugLevel");
  string drawLevel = acf.GetString("drawLevel");

  int nEvents = acf.GetInteger("nEvents");

  //smearing parameters
  int step = acf.GetInteger("step");
  double sigma = acf.GetDouble("sigma");

  auto xl = acf.GetDouble("xl");
  auto xu = acf.GetDouble("xu");
  auto yl = acf.GetDouble("yl");
  auto yu = acf.GetDouble("yu");
  auto zl = acf.GetDouble("zl");
  auto zu = acf.GetDouble("zu");

  // done reading data from xml file ---------

  // general debug level
  SetDebugLevel(debugLevel);

  //read data file
  string fp = FilePath(dataPath,fileName);

  irene::Event* ievt = new irene::Event();
  TFile* ifile = new TFile(fp.c_str(), "READ");
  TTree* fEvtTree = dynamic_cast<TTree*>(ifile->Get("EVENT"));
  fEvtTree->SetBranchAddress("EventBranch", &ievt);

  auto nRun = std::min(nEvents, (int) fEvtTree->GetEntries());

  cout << "number of entries in Irene Tree = " << fEvtTree->GetEntries() << endl;
  cout << "number of events required = " << nEvents << endl;
  cout << "number of events to run  = " << nRun << endl;
      
  vector<double> vsigma;
  vsigma.push_back(sigma); // sigma_x
  vsigma.push_back(sigma); // sigma_y
  vsigma.push_back(0.01); // sigma_z ~0

  vector<double> hl; 
  vector<double> hu;

  hl.push_back(xl);
  hl.push_back(yl);
  hl.push_back(zl);

  hu.push_back(xu);
  hu.push_back(yu);
  hu.push_back(zu);

  int rblob = 10;

  auto isvc = ISvc(step,vsigma,rblob);

  cout << "ISVC--->" << isvc << endl;
  
  string input ;
  cout << "Return to continue:\n>";
  getline(cin, input);

  //init Paolina

  int nb;
  std::vector<const paolina::Voxel*> pvoxels; 
  //Voxel builder

  //blob radius
  double Rblob = 20; // in mm

  
  std::vector<double> pvoxel_size;
  std::pair<double,double> xrange;
  std::pair<double,double> yrange;
  std::pair<double,double> zrange; 
  std::vector<std::pair<double,double> > vol_range;
    
  double voxel_x = 5; // in mm
  double voxel_y = 5; // in mm
  double voxel_z = 5; // in mm

  pvoxel_size.push_back(voxel_x); //voxel size
  pvoxel_size.push_back(voxel_y);
  pvoxel_size.push_back(voxel_z);

  double world_xl = -300; // in mm
  double world_yl = -300; // in mm
  double world_zl = -300; // in mm

  double world_xr = 300; // in mm
  double world_yr = 300; // in mm
  double world_zr = 300; // in mm

  printf("Define data 2\n");
  xrange.first=world_xl;
  xrange.second=world_xr;
  yrange.first=world_yl;
  yrange.second=world_yr;
  zrange.first=world_zl;
  zrange.second=world_zr;

  vol_range.push_back(xrange);
  vol_range.push_back(yrange);
  vol_range.push_back(zrange);
  paolina::VoxelBuilder* pVB = new paolina::VoxelBuilder(pvoxel_size, vol_range);

  // //paolina tracks
  // std::vector<paolina::Track*> ptracks;

  // //paolina track builder
  // paolina::TrackBuilder* pTB =new paolina::TrackBuilder();

  // //paolina blobs
  // std::pair<paolina::Blob*, paolina::Blob*> pblobs;

  // //paolina Blob builder
  // paolina::BlobBuilder* pBB = new paolina::BlobBuilder(fRblob);

 
  cout << "Return to continue:\n>";
  getline(cin, input);

  //-----------Event loop --------
  for (int ivt = 0; ivt < nRun; ivt++)
  {
    nb = fEvtTree->GetEntry(ivt);

    isvc.LoadEvent(ievt);

    klog << log4cpp::Priority::INFO 
        << "reading event  " << ivt;
    klog << log4cpp::Priority::INFO 
        << "reading " << nb  << "bytes from tree"; 

     

    isvc.DrawHits("True", hl, hu);
    

    cout << "Return to continue:\n>";
    getline(cin, input);

    // Paolina

    std::vector<std::pair<TLorentzVector,double> > ihits_nexus;

    ievt->FillHitVector(ihits_nexus, "ACTIVE");

    std::vector<paolina::Voxel*> voxels = pVB->FillVoxels(ihits_nexus); 
    

    klog << log4cpp::Priority::INFO 
        << " voxel vector size  " << voxels.size() << "\n"; 
    cout << "Return to continue:\n>";

    getline(cin, input);

    DrawVoxels(voxels, hl,  hu) ;

  }
 
 
  //------------
    theApp->Run();
    return 0;
  }
  
//--------------------------------------------------------------------
  void DrawVoxels(std::vector<paolina::Voxel*> voxels, vector<double> hl, vector<double> hu) 
//--------------------------------------------------------------------
  {
   
    TCanvas *c1 = new TCanvas( "c1", "Hits", 200, 10, 600, 800 );
    gStyle->SetOptStat(1000000001);

    auto nhit = voxels.size();
    
    auto xl = hl[0];
    auto xu = hu[0];
    auto yl = hl[1];
    auto yu = hu[1];
    auto zl = hl[2];
    auto zu = hu[2];

    auto hxz = new TH2F("hxz", "Voxels: x vs z", nhit, zl, zu, nhit, xl, xu);
    auto hyz =  new TH2F("hyz", "Voxels: y vs z", nhit, zl, zu, nhit, yl, yu);
    auto hxy =  new TH2F("hxy", "Voxels: x vs y", nhit, xl, xu, nhit, yl, yu);

    for (auto it=voxels.begin(); it!=voxels.end(); ++it)
    {
      auto voxel = *it;
      TVector3 xyzt = voxel->posvector();
      double energy = voxel->EDep();
      
       hxz->Fill(xyzt.Z(), xyzt.X(), energy);
       hyz->Fill(xyzt.Z(), xyzt.Y(), energy);
       hxy->Fill(xyzt.X(), xyzt.Y(), energy);
    }

    
    c1->Divide(1,3);
    
    c1->cd(1);
    hxz->Draw("colz");
    c1->cd(2);
    hyz->Draw("colz");
    c1->cd(3);
    hxy->Draw("colz");
    c1->Update();

    string input ;
    cout << "Return to continue:\n>";
    getline(cin, input);

    delete hxz;
    delete hyz;
    delete hxy;
    delete c1;
 

  }
  
